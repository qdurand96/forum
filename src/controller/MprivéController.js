import {
    Router
} from "express";
import {
    MessagesPriveRepository
} from "../repository/MPriveRepository";

export const mPrivéController = Router();


mPrivéController.put('/add', async (req,res)=>{
    await MessagesPriveRepository.addMessagePrivé(req.body);
    res.end();
});

mPrivéController.delete('/delete/:id', async (req,res)=>{
    await MessagesPriveRepository.deleteMessagePrivé(req.params.id);
    res.end();
});

mPrivéController.put('/update', async (req,res)=>{
    await MessagesPriveRepository.updateMessagePrivé(req.body);
    res.end();
});

mPrivéController.get('/message/:id', async (req,res)=>{
    let message = await MessagesPriveRepository.getOneMessagePrivé(req.params.id);
    res.json(message);
});

mPrivéController.get('/user/:id', async (req,res)=>{
    let messages = await MessagesPriveRepository.getAllMsgFromUser(req.params.id);
    res.json(messages);
});

mPrivéController.get('/conv/:id', async(req,res)=>{
    let messages = await MessagesPriveRepository.getConversation(req.params.id);
    res.json(messages);
});
