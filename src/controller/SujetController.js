import {
    Router
} from "express";
import { SujetRepository } from "../repository/SujetRepository";

export const sujetController = Router();

sujetController.get('/all', async(req, res) => {
    let sujets = await SujetRepository.getAllSujets();
    if (!sujets) {
        res.status(404).json({ error: 'Not Found' })
        return;
    }
    res.json(sujets)
})

sujetController.get('/sujet/:id', async(req, res) => {
    let sujet = await SujetRepository.getOneSujet(req.params.id);
    if (!sujet) {
        res.status(404).json({ error: 'Not Found' })
        return;
    }
    res.json(sujet)
})

sujetController.delete('/delete/:id', async(req, res) => {
    await SujetRepository.deleteOneSujet(req.params.id);
    res.end();
})

sujetController.put('/update', async(req, res) => {
    await SujetRepository.updateOneSujet(req.body);
    res.end();
})

sujetController.put('/add', async(req, res) => {
    await SujetRepository.addSujet(req.body);
    res.end();
})