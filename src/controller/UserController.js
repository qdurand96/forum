import {
    Router
} from "express";
import { UserRepository } from "../repository/UserRepository";

export const userController = Router();

userController.get('/user/:id', async(req, res) => {
    let user = await UserRepository.getOneUser(req.params.id);
    if (!user) {
        res.status(404).json({ error: 'Not Found' })
        return;
    }
    res.json(user)
})

userController.get('/all', async(req, res) => {
    let users = await UserRepository.getAllUsers();
    if (!users) {
        res.status(404).json({ error: 'Not Found' })
        return;
    }
    res.json(users)
})

userController.delete('/delete/:id', async(req, res) => {
    await UserRepository.deleteOneUser(req.params.id);
    res.end();
})


userController.put('/update', async(req, res) => {
    await UserRepository.updateOneUser(req.body);
    res.end();
})

userController.put('/add', async(req, res) => {
    await UserRepository.addUser(req.body);
    res.end();
})