import {
    Router
} from "express";
import {
    MessagePublicRepository
} from "../repository/MPublicRepository";

export const mPublicController = Router();

mPublicController.get('/message/:id', async (req,res)=>{
    let message = await MessagePublicRepository.getOneMessagePublic(req.params.id);
    res.json(message);
});

mPublicController.get('/user/:id', async (req,res)=>{
    let messages = await MessagePublicRepository.getAllMsgFromUser(req.params.id);
    res.json(messages);
});

mPublicController.get('/sujet/:id', async (req,res)=>{
    let messages = await MessagePublicRepository.getAllMsgFromSujet(req.params.id);
    res.json(messages);
});   

mPublicController.put('/add', async (req,res)=>{
    await MessagePublicRepository.addMessagePublic(req.body);
    res.end();
});

mPublicController.delete('/delete/:id', async (req,res)=>{
    await MessagePublicRepository.deleteMessagePublic(req.params.id);
    res.end();
});

mPublicController.put('/update', async (req,res)=>{
    await MessagePublicRepository.updateMessagePublic(req.body);
    res.end();
});