import "dotenv-flow/config";
import { UserRepository } from "./repository/UserRepository";


import { server } from "./server";

const port = process.env.PORT || 3000;



server.listen(port, () => {
    console.log('listening on http://localhost:'+port);
})

process.on('SIGINT', () => { 
    console.log("exiting…"); 
    process.exit(0); 
});

process.on('exit', () => { 
    console.log("exiting…"); 
    process.exit(0); 
});

