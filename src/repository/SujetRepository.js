import { connection } from "./connection";
import { Sujet } from "../entity/Sujet";

export class SujetRepository {
    
    static async getAllSujets() {
        const [rows] = await connection.execute(`SELECT * FROM Sujet`);

        let sujets = [];
        for (let item of rows) {
            let sujet = new Sujet(item.titre, item.theme, item.date, item.id)
            sujets.push(sujet);
        }

        return sujets;
    }

    static async getOneSujet(id) {
        const [rows] = await connection.execute('SELECT * FROM Sujet WHERE id=?', [id]);

        if (rows.length === 1) {
            return new Sujet(rows[0].titre, rows[0].theme, rows[0].date, rows[0].id,rows[0].user_id);
        }
        return null;
    }

    static async addSujet(sujet) {
        await connection.execute("INSERT INTO `Sujet` (titre, theme, date, user_id) VALUES (?, ?, ?, ?)", [sujet.titre,sujet.theme, sujet.date, sujet.user_id])
    }

    static async deleteOneSujet(id) {
        await connection.execute('DELETE FROM Sujet WHERE id=?', [id]);
    }

    static async updateOneSujet(dataUpdate) {
        await connection.execute('UPDATE Sujet SET titre=?, theme=?, date=?, user_id=? WHERE id=?', [dataUpdate.titre, dataUpdate.theme, dataUpdate.date, dataUpdate.user_id, dataUpdate.id]);
    }
}