import { connection } from "./connection";
import {MessagePrive} from "../entity/Message_privé";

export class MessagesPriveRepository {

    static async getAllMessagesPrivés() {
        const [rows] = await connection.query('SELECT * FROM `Message privé`');

        let messagesPrivés = [];
        for (let item of rows) {
            let messagePrivé = new MessagePrive(item.texte, item.date,item.conv_id,item.user_id, item.id)
            messagesPrivés.push(messagePrivé);
        }
        return messagesPrivés;
    }

    static async getConversation(id){
        const [rows] = await connection.query('SELECT * FROM `Message privé` WHERE conv_id=?', [id]);

        let messages = [];
        for (let item of rows) {
            let message = new MessagePrive(item.texte, item.date,item.conv_id, item.user_id, item.id)
            messages.push(message);
        }
        return messages;
    }

    static async getAllMsgFromUser(id) {
        const [rows] = await connection.query('SELECT * FROM `Message privé` WHERE user_id=?', [id]);

        let messages = [];
        for (let item of rows) {
            let message = new MessagePrive(item.texte, item.date, item.conv_id,item.user_id, item.id)
            messages.push(message);
        }
        return messages;
    }

    static async getOneMessagePrivé(id) {
        const [rows] = await connection.query('SELECT * FROM `Message privé` WHERE id=?', [id]);

        if (rows.length === 1) {
            return new MessagePrive(rows[0].texte, rows[0].date,rows[0].conv_id, rows[0].user_id, rows[0].id);
        }
        return null;
    }

    static async addMessagePrivé(message) {
        await connection.query("INSERT INTO `Message privé` (texte, date, conv_id,user_id) VALUES (?,?,?,?)", [message.texte, message.date, message.conv_id, message.user_id])
    }

    static async deleteMessagePrivé(id) {
        await connection.query('DELETE FROM `Message privé` WHERE id=?', [id]);
    }

    static async updateMessagePrivé(dataUpdate) {
        await connection.query('UPDATE `Message privé` SET texte=?, date=?, conv_id=?, user_id=? WHERE id=?', [dataUpdate.texte, dataUpdate.date, dataUpdate.conv_id, dataUpdate.user_id, dataUpdate.id]);
    }
}

