import { connection } from "./connection";
import {MessagePublic} from "../entity/Message_public";

export class MessagePublicRepository {

    static async getAllMessagesPublics() {
        const [rows] = await connection.execute('SELECT * FROM `Message public`');
        let messagesPublics = [];
        for (let item of rows) {
            let messagePublic = new MessagePublic(item.texte, item.date, item.id)
            messagesPublics.push(messagePublic);
        }
        return messagesPublics;
    }

    static async getOneMessagePublic(id) {
        const [rows] = await connection.execute('SELECT * FROM `Message public` WHERE id=?', [id]);

        if (rows.length === 1) {
            return new MessagePublic(rows[0].texte, rows[0].date, rows[0].id, rows[0].user_id, rows[0].sujet_id);
        }
        return null;
    }

    static async getAllMsgFromUser(id) {
        const [rows] = await connection.execute('SELECT *  FROM `Message public` WHERE user_id=?', [id]);

        let messagesPublics = [];
        for (let item of rows) {
            let messagePublic = new MessagePublic(item.texte, item.date, item.id, item.user_id,item.sujet_id)
            messagesPublics.push(messagePublic);
        }
        return messagesPublics;
    } 

    static async getAllMsgFromSujet(id) {
        const [rows] = await connection.execute('SELECT *  FROM `Message public` WHERE sujet_id=?', [id]);

        let messagesPublics = [];
        for (let item of rows) {
            let messagePublic = new MessagePublic(item.texte, item.date, item.id, item.user_id,item.sujet_id)
            messagesPublics.push(messagePublic);
        }
        return messagesPublics;
    }

    static async addMessagePublic(message) {
        await connection.execute("INSERT INTO `Message public` (texte, date, user_id, sujet_id) VALUES (?,?,?,?)", [message.texte, message.date, message.user_id, message.sujet_id]);
        res.end();
    }

    static async deleteMessagePublic(id) {
        await connection.execute('DELETE FROM `Message public` WHERE id=?', [id]);
        res.end();
    }

    static async updateMessagePublic(dataUpdate) {
        await connection.execute('UPDATE `Message public` SET texte=?, date=? WHERE id=?', [dataUpdate.texte, dataUpdate.date, dataUpdate.id]);
        res.end();
    }
}