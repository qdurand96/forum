import { connection } from "./connection";
import { User } from "../entity/User";

export class UserRepository {

    static async getAllUsers() {
        let [rows] = await connection.execute(`SELECT * FROM User`);
        let users = [];
        for (let item of rows) {
            let user = new User(item.pseudo, item.biographie, item.localisation, item.email, item.role, item.mdp, item.id)
            users.push(user);
        }
        return users;
    }

    static async getOneUser(id) {
        const [rows] = await connection.execute('SELECT * FROM User WHERE id=?', [id]);
        if (rows.length === 1) {
            return new User(rows[0].pseudo, rows[0].biographie, rows[0].localisation,
                rows[0].email, rows[0].role, rows[0].mdp, rows[0].id);
        }
        return null;
    }

    static async addUser(user) {
        await connection.execute("INSERT INTO `User` (email,mdp,pseudo,role,biographie,localisation) VALUES (?, ?, ?, ?, ?, ?)", [user.email, user.mdp, user.pseudo,user.role, user.biographie,user.localisation])
    }

    static async deleteOneUser(id) {
        await connection.execute('DELETE FROM User WHERE id=?', [id]);
    }

    static async updateOneUser(dataUpdate) {
        await connection.execute('UPDATE User SET pseudo=?, biographie=?, localisation=?, email=?, mdp=?, role=? WHERE id=?', [dataUpdate.pseudo, dataUpdate.biographie, dataUpdate.localisation, dataUpdate.email, dataUpdate.mdp, dataUpdate.role, dataUpdate.id]);
    }

}