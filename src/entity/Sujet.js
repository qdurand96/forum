export class Sujet {
    titre;
    theme;
    date;
    id;
    user_id;
    constructor(titre, theme, date, id=null,user_id) {
        this.titre = titre;
        this.theme = theme;
        this.date = date;
        this.id = id;
        this.user_id = user_id;
    }
}