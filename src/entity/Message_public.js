export class MessagePublic {
    texte;
    date;
    id;
    user_id;
    sujet_id;
    constructor(texte, date, id,user_id,sujet_id) {
        this.texte = texte;
        this.date = date;
        this.id = id;
        this.user_id=user_id;
        this.sujet_id=sujet_id;
    }
}