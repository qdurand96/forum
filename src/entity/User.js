export class User {
    pseudo;
    biographie;
    localisation;
    email;
    role;
    mdp;
    id;
    constructor(pseudo, biographie, localisation, email, role, mdp, id) {
        this.pseudo = pseudo;
        this.biographie = biographie;
        this.localisation = localisation;
        this.email = email;
        this.role = role;
        this.mdp = mdp;
        this.id = id;
    }
}