import express from 'express';
import { mPrivéController } from './controller/MprivéController';
import { mPublicController } from './controller/MPublicController';
import { sujetController } from './controller/SujetController';
import { userController } from './controller/UserController';
export const server = express();

server.use(express.json());

server.use('/api/messagesPrives', mPrivéController);
server.use('/api/messagesPublics', mPublicController);
server.use('/api/sujets', sujetController);
server.use('/api/users', userController);
server.get('/', (req, res)=>{
    res.json({
        message: "Hello Jean & Catherine"
    })
})
