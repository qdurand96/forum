DROP DATABASE IF EXISTS `Forum`;
CREATE DATABASE Forum;
USE promo14_QuenDan_forum;

DROP TABLE IF EXISTS `User`;
CREATE TABLE `User` (
    `id` int(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,
    `email` VARCHAR (320) NOT NULL,
    `mdp` VARCHAR (24) NOT NULL,
    `pseudo` VARCHAR(16) NOT NULL,
    `role` VARCHAR (24) NOT NULL,
    `biographie` VARCHAR (500),
    `localisation` VARCHAR (64)
    
) DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS `Sujet`;
CREATE TABLE  `Sujet` (
    `id` int(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,
    `titre` VARCHAR(200) NOT NULL ,
    `theme` VARCHAR (200) NOT NULL ,
    `date` DATETIME ,
    `user_id` int(11) DEFAULT NULL ,
    FOREIGN KEY (`user_id`) REFERENCES `User` (`id`) ON DELETE SET NULL
) DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS `Message public`;
CREATE TABLE `Message public`(
    `id` int(11) PRIMARY KEY NOT NULL AUTO_INCREMENT ,
    `texte` VARCHAR (2000) NOT NULL ,
    `date` DATETIME NOT NULL,
    `user_id` int(11) DEFAULT NULL ,
    `sujet_id` int(11) DEFAULT NULL ,
    FOREIGN KEY (`user_id`) REFERENCES `User` (`id`) ON DELETE SET NULL,
    FOREIGN KEY (`sujet_id`) REFERENCES `Sujet` (`id`) ON DELETE SET NULL
)DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS `Message privé`;
CREATE TABLE `Message privé`(
    `id` int(11) PRIMARY KEY NOT NULL AUTO_INCREMENT ,
    `texte` VARCHAR (2000) NOT NULL ,
    `date` DATETIME NOT NULL,
    `conv_id` int(11) NOT NULL,
    `user_id` int(11) DEFAULT NULL ,
        FOREIGN KEY (`user_id`) REFERENCES `User` (`id`) ON DELETE SET NULL
)DEFAULT CHARSET=utf8mb4;


-- User
INSERT INTO `User` (email,mdp,pseudo,role,biographie, localisation) VALUES (
    'simplon@simplon.com','1234','Frenc','admin','coucou','Charvieu'
);
INSERT INTO `User` (email,mdp,pseudo,role,biographie, localisation) VALUES (
    'simplon@simplon.com','1234','Davina','modérateur','couille','Portugal'
);
INSERT INTO `User` (email,mdp,pseudo,role,biographie, localisation) VALUES (
    'simplon@simplon.com','1234','Hedgehog','membre','CHIEN!!!','Lyon'
);

--Topics
INSERT INTO `Sujet` (titre,theme,date,user_id) VALUES (
    'Metallica: sont ils trop vieux?','Trash','2021-01-01 12:12:12', 1
);
INSERT INTO `Sujet` (titre,theme,date,user_id) VALUES (
    'Marylin Manson, son procès de A à Z','Metal Alternatif','2021-03-01 17:13:45', 2
);
INSERT INTO `Sujet` (titre,theme,date,user_id) VALUES (
    'Ozzy Osbourne et les chauve-souris','Metal','2021-03-01 21:22:54', 3
);

--Topic 1 msg
INSERT INTO `Message public` (texte,date,user_id,sujet_id) VALUES (
    'On se demande comment ces dinosaures tiennent encore debout', '2021-01-01 12:12:12', 1, 1
);
INSERT INTO `Message public` (texte,date,user_id,sujet_id) VALUES (
    'Basiquement ils sont encore capable de faire des concerts', '2021-01-01 12:15:56', 2, 1
);
INSERT INTO `Message public` (texte,date,user_id,sujet_id) VALUES (
    'Les pauuuvres', '2021-01-01 12:45:54', 3, 1
);

--Topic 2 msg
INSERT INTO `Message public` (texte,date,user_id,sujet_id) VALUES (
    'Marylin Manson a été inculpé pour diverses accusations venant de ses anciennes compagnes', 
    '2021-03-01 17:13:45', 2, 2
);
INSERT INTO `Message public` (texte,date,user_id,sujet_id) VALUES (
    'Il est bizarre ce gars', '2021-03-01 17:15:32', 3, 2
);
INSERT INTO `Message public` (texte,date,user_id,sujet_id) VALUES (
    'So what?', '2021-03-01 17:13:45', 1, 2
);

--Topic 3 msg
INSERT INTO `Message public` (texte,date,user_id,sujet_id) VALUES (
    'Mange-t-il encore des chauve-souris?', '2021-03-01 21:22:54', 3, 3
);
INSERT INTO `Message public` (texte,date,user_id,sujet_id) VALUES (
    'Jsp', '2021-03-01 21:28:44', 2, 3
);
INSERT INTO `Message public` (texte,date,user_id,sujet_id) VALUES (
    'Et vous?', '2021-03-01 21:42:23', 1, 3
);

--Messages privés
--Conv 1
INSERT INTO `Message privé` (texte, date,conv_id,user_id) VALUES (
    'Bonsoir, ça va? Tu vends encore des billets pour le hellfest?',
    '2021-06-11 18:20:37',1,3
);
INSERT INTO `Message privé` (texte, date,conv_id,user_id) VALUES (
    'Bonsoir, oue mais juste pour une journée',
    '2021-06-11 18:43:10',1,2
);
INSERT INTO `Message privé` (texte, date,conv_id,user_id) VALUES (
    "Bon ok, je pensais que c'était pour tout le fest... je vais faire un topic pour demande si qqun a des billets",
    '2021-06-11 19:02:25',1,3
);
INSERT INTO `Message privé` (texte, date,conv_id,user_id) VALUES (
    "Pas de soucis, toute façons si tu trouves pas et si t'es encore interessé je suis là",
    '2021-06-11 19:08:45',1,2
);
INSERT INTO `Message privé` (texte, date,conv_id,user_id) VALUES (
    'merci \m/',
    '2021-06-11 19:11:05',1,3
);

--Conv 2 
INSERT INTO `Message privé` (texte, date,conv_id,user_id) VALUES (
    "eh mec, t'as vu le nouvel album d Abigor?",
    '2021-06-13 10:21:15',2,1
);
INSERT INTO `Message privé` (texte, date,conv_id,user_id) VALUES (
    'non! il est déjà sortie?',
    '2021-06-13 10:23:30',2,3
);
INSERT INTO `Message privé` (texte, date,conv_id,user_id) VALUES (
    "oui! ça fait 6 mois!! putain je l'ai rate!!!",
    '2021-06-13 10:27:02',2,1
);
INSERT INTO `Message privé` (texte, date,conv_id,user_id) VALUES (
    "ahaha c'est pas possible! tu l as ecoute deja?",
    '2021-06-13 10:30:21',2,3
);
INSERT INTO `Message privé` (texte, date,conv_id,user_id) VALUES (
    "ah oue, c'est mortel!!",
    '2021-06-13 10:33:53',2,1
);
INSERT INTO `Message privé` (texte, date,conv_id,user_id) VALUES (
    "j'y vais l'écoute maintenant!!",
    '2021-06-13 10:35:42',2,3
);


--Conv 3
INSERT INTO `Message privé` (texte, date,conv_id,user_id) VALUES (
    "tu m'accompagnes au concert de Viagra Boys??",
    '2021-06-20 22:40:02',3,2
);
INSERT INTO `Message privé` (texte, date,conv_id,user_id) VALUES (
    'ils jouent?? :O quand et ou?',
    '2021-06-21 08:46:48',3,3
);
INSERT INTO `Message privé` (texte, date,conv_id,user_id) VALUES (
    'à lyon le 25 nov, au transbo',
    '2021-06-21 11:14:23',3,2
);
INSERT INTO `Message privé` (texte, date,conv_id,user_id) VALUES (
    'chouette!! et le ticket?',
    '2021-06-21 11:22:55',3,3
);
INSERT INTO `Message privé` (texte, date,conv_id,user_id) VALUES (
    "22e mais j't'offre l ticket!",
    '2021-06-21 11:27:33',3,2
);
INSERT INTO `Message privé` (texte, date,conv_id,user_id) VALUES (
    'olala mais quoi, je suis pas ta meuf!? ahaha',
    '2021-06-21 11:30:07',3,3
);
INSERT INTO `Message privé` (texte, date,conv_id,user_id) VALUES (
    'ta gueule! ça fait 6 mois que je te vois pas!',
    '2021-06-21 11:35:12',3,2
);

