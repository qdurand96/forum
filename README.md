# Forum

For our project Forum, we decided to go with the  music thematic.

The functionalities we have are:

**-> as a visitor** \
-See the forum \
-Subscribe to the forum \
-Search on the forum 

**-> as a member** \
-Login \
-Send a private message \
-Search \
-Create a topic \
-Post a message/comment \
-Edit the message/comment \
-Delete the message/comment 

**-> as a admin** \
-Login \
-Send a private message \
-Search \
-Create a topic \
-Post a message \
-Delete messages and topics \
-Ban user temporarily/permanently 

**Note** \
Not all the methods are used since they wouldn't be used in a forum, but we still let them in the code. \

**This is our maquette ->**

![alt text](public/img/Maquettes.png)

**This is our user stories ->**

![alt text](public/img/Visite_User_Story.png)
![alt text](public/img/Admin_User_Story.png)
![alt text](public/img/Message_User_Story.png)
![alt text](public/img/MP_User_Story.png)

**This our class diagram ->**

![alt text](public/img/Diagramme_de_classes.png)

**Link:** <https://forum-quendan.herokuapp.com>
